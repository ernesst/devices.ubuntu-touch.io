---
codename: 'arale'
name: 'Meizu MX4'
comment: 'legacy'
icon: 'phone'
image: 'https://4.bp.blogspot.com/-1X6u1vxxRlQ/VV45bb_6CpI/AAAAAAAAlMg/iQ62alhMxo0/s1600/u-t.png'
maturity: .9
---

## Pursuit of the ultimate perfection

On the top-end of the price-range of the previously available Ubuntu Touch devices, the Meizu MX 4 combines an elegant operating system with the hardware it deserves to run on.

**Note**: Meizu MX 4 devices that are sold with Android have a locked bootloader, so those need to be [manually unlocked by installing the manufacturers Ubuntu image](https://docs.ubports.com/en/latest/userguide/install.html#install-on-legacy-android-devices) before switching to UBports' release of Ubuntu Touch.
