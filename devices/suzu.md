---
codename: 'suzu'
name: 'Sony Xperia X (F5121 & F5122)'
comment: 'community device'
icon: 'phone'
maturity: .8
---

You can install Ubuntu Touch on the versions F5121 and F5122 of the Sony Xperia X.